package com.example.demo;

public class Duck extends Animals {
	public Duck(int age, String gender, String beakColor) {
		this.age = age;
		this.gender = gender;
		this.beakColor = beakColor;
	}

	public Duck() {
		this.age = 1;
		this.gender = "female";
		this.beakColor = "yellow";
	}

	private String beakColor;

	@Override
	public boolean isMammal() {
		return false;
	}

	@Override
	public void mate() {
		System.out.println("Duck mate");
	}

	public void siwm() {
		System.out.println("Duck swimming");
	}

	public void quack() {
		System.out.println("Duck quack");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

}
