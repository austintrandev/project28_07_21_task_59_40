package com.example.demo;

import java.util.ArrayList;

public class Student extends Person implements ISchool {

	public Student(int age, String gender, String name, Address address,int studentId,ArrayList<Subject> listSubject, ArrayList<Animals> listPet) {
		super(age, gender, name, address, listPet);
		// TODO Auto-generated constructor stub
		this.studentId = studentId;
		this.listSubject = listSubject;
	}

	
	public Student() {
		this.studentId = 123;
		this.listSubject = listSubject;
	}

	private int studentId;
	private ArrayList<Subject> listSubject;

	public void doHomework() {
		System.out.println("Student is doing homework!");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Student eats Spagetti");
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public ArrayList<Subject> getListSubject() {
		return listSubject;
	}

	public void setListSubject(ArrayList<Subject> listSubject) {
		this.listSubject = listSubject;
	}

}
