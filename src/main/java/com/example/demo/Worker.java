package com.example.demo;

import java.util.ArrayList;

public class Worker extends Person {
	public Worker(int age, String gender, String name, Address address, int salary, ArrayList<Animals>listPet) {
		super(age, gender, name, address, listPet);
		this.salary = salary;
	}

	public Worker() {
		this.salary = 20000000;
	}

	private int salary;

	public void working() {
		System.out.println("Worker is working");
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public void eat() {
		System.out.println("Worker eats pizza");
		// TODO Auto-generated method stub

	}

}
